'use strict';

var Config = {
	server: {
		protocol: 'http://',
		adress: 'localhost',
		port: 2222
	}, 
	db: {
		adress: 'mongodb://localhost',
		port: '27017',
		name: 'contacts_ng2'
	}
};

module.exports = Config;