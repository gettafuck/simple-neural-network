'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
	name: String,
	surName: String,
	city: String,
	age: Number,
	imgUrl: String
});

const User = mongoose.model('User', UserSchema);

module.exports = User;
