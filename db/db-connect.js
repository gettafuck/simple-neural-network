module.exports = () => {
	'use strict';
	const config = require('./../config');
	const mongoose = require('mongoose');
	mongoose.Promise = global.Promise;
	mongoose.connect(config.db.adress + ':' + config.db.port + '/' + config.db.name);
	const db = mongoose.connection;
	db.on('error', console.error.bind(console, 'db connection error:'));
	db.once('open', () => {
		console.log("Successfully connected to MongoDB");
	});
}