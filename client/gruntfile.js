module.exports = (grunt) => {
	grunt.initConfig({
		concat: {
			css: {
				src: ['styles/src/*.scss', '!styles/src/merged-styles.scss'],
				dest: 'styles/src/merged-styles.scss'
			}
		},
		sass: {
		    dist: {
				files: [{
					expand: true,
					cwd: 'styles/src/',
					src: ['merged-styles.scss'],
					dest: 'styles/dist/',
					ext: '.css'
				}]
		    }
		},
		cssmin: {
			my_target: {
				files: [{
					expand: true,
					cwd: 'styles/dist/',
					src: ['merged-styles.css'],
					dest: 'styles/dist/',
					ext: '.min.css'
				}]
			}
		},
		watch: {
			css: {
				files: 'styles/src/*.scss',
				tasks: ['concat', 'sass', 'cssmin']
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-watch');
}