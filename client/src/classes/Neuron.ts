import {INeuron} from '../interfaces/INeuron';

export class Neuron implements INeuron {
	neuronId:string;
	constructor(neuronId:string) {
		this.neuronId = neuronId;
	}

	sendSignal( targetNeuronId:string ):Boolean {
		return true;
	}
}