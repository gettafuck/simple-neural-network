'use strict';

const bodyParser = require('body-parser');
const express = require('express');

const config = require('./config');

const app = express();
const server = app.listen(config.server.port, () => {
	console.log('Server listening at ' +
		config.server.protocol + config.server.adress + ':' + config.server.port);
});

require('./db/db-connect')();
const User = require('./db/models/user');

/*******
* Routes *
********/

/** others **/
	app.use('/', express.static(__dirname + '/client'));
	app.use('/uploads', express.static(__dirname + '/uploads'));

	app.use(bodyParser.json());

/** Users **/
	app.post('/users/:amount', (request, response) => {
		let amount = request.params.amount * 1;
		let names = ['Dylan', 'Bob', 'Mike', 'Tim', 'Torry', 'Jake', 'Johny', 'Sam', 'Luis', 'Derek', 'Alan'];
		let surNames = ['Smith', 'Doe', 'Rieder', 'Pudwill', 'Lake', 'Li', 'Fill', 'McDonald', 'Ake', 'Anderson', 'Lyhn'];
		let cities = ['London', 'Liverpool', 'New York', 'Tokio', 'Berlin', 'Manchester', 'Dublin', 'Oslo', 'Stokholm', 'Amsterdam', 'Dortmut'];
		let getRandInt = (min, max) => {
			return Math.floor(Math.random() * (max - min + 1)) + min;
		}
		let users = [];
		for(let i = 0; i < amount; i++) {
			let newUser = {
				name: names[ getRandInt(0, 10) ],
				surName: surNames[ getRandInt(0, 10) ],
				city: cities[ getRandInt(0, 10) ],
				age: getRandInt(15, 30),
				imgUrl: '/uploads/img/users/' + getRandInt(1, 11) + '.jpg'
			};
			users.push(newUser);
		}

		User.collection.dropIndex('login_1'); // fix of error
											  // 'E11000 duplicate key error collection: contacts_ng2.users index: login_1 dup key: { : null }'
		User.collection.insert(users, (err, doc) => {
			if(err) {
		  		console.log('/users/:amount | POST | Error was occurred');
				console.log(err.errmsg);
				response.status(403).send(err.errmsg);
			}
			if(!err) {
				response.status(200).send(amount + ' users was created');
			}
		});
	});

	app.get('/users', (request, response) => {
		User.find((err, docs) => {
			if (err) {
		  		console.log('/users | GET | Error was occurred');
		  		response.status(403).send(err.errmsg);
		  	}
		  	if(docs)
				response.status(200).send(docs);
		});
	});

	app.delete('/users', (request, response) => {
		User.collection.remove((err) => {
			if (err) {
		  		console.log('/users | DELETE | Error was occurred');
		  		response.status(403).send(err.errmsg);
		  	}
		  	if(!err)
				response.status(200).send('collection users was removed');
		});
	});